import React, { useEffect, useState } from "react";

import { Button, Image, Table } from "antd";
import { FileExcelOutlined, UserAddOutlined } from "@ant-design/icons";
import { BsArrowRightCircle } from "react-icons/bs";
import { Link, useNavigate } from "react-router-dom";
import { API_ROOT } from "../const";
import axios from "axios";
const COLUMNS = [
  {
    title: "Багшийн дугаар",
    dataIndex: "teacherid",
  },
  {
    title: "Нэр",
    dataIndex: "teachername",
  },
  {
    title: "Утас",
    dataIndex: "phone",
  },
  {
    render: ({ teacherid }) => {
      return (
        <Link to={`edit/${teacherid}`}>
          <BsArrowRightCircle style={{ fontSize: 25 }} />
        </Link>
      );
    },
  },
];
const List = () => {
  const navigate = useNavigate();
  const [list, setlist] = useState(null);
  useEffect(() => {
    fetch();
  }, []);
  const fetch = async () => {
    let response = await axios({
      method: "POST",
      url: "/teacher/list",
    });
    setlist(response.data.data);
    console.log(
      "🚀 ~ file: List.js ~ line 21 ~ fetch ~ response.data",
      response.data
    );
  };
  return (
    <div className="flex flex-col  p-2">
      <div className="flex flex-row justify-between items-center bg-white p-3 border-l-8 border-def-blue mb-4">
        <div className="text-base font-semibold">Ажилчдын удирдлага</div>
        <div className="flex-row">
          <Link to={"new"}>
            <Button
              type="primary"
              className="ml-2"
              icon={<UserAddOutlined />}
              size="middle"
              shape="circle"
              onClick={() => navigate("/new")}
            ></Button>
          </Link>
        </div>
      </div>
      {list && <Table dataSource={list} columns={COLUMNS} />}
    </div>
  );
};

export default List;
