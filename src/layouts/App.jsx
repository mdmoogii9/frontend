import React, { useState } from "react";
import { UserOutlined } from "@ant-design/icons";
import { Avatar, Badge, Spin } from "antd";
import { NavLink, Route, Routes } from "react-router-dom";
import { useAuth } from "../utils/auth";
import {
  QuestionCircleOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";
//Хэрэглэгчид
import UserForm from "../user/Form";
import UserList from "../user/List";
const App = () => {
  const [showNoti, setshowNoti] = useState(false);
  const { user, logout } = useAuth();

  if (!user)
    return (
      <div className="flex w-full h-screen justify-center align-middle items-center ">
        <Spin size="large" />
      </div>
    );

  return (
    <div className="flex flex-row bg-def-gray ">
      <div className="flex flex-row ">
        <div className="flex flex-col mt h-screen bg-white border-r">
          <div
            className="flex flex-row justify-center py-3 align-middle items-center "
            onClick={() => console.log("1")}
          >
            <img
              src="../img/logo.png"
              style={{ width: 40, height: 40 }}
              alt=""
            />
          </div>
          <div className="flex flex-col justify-between  h-full">
            <div>
              <NavLink
                to={"user"}
                className={({ isActive }) =>
                  `${
                    isActive ? `bg-def-blue text-white` : `text-black`
                  } flex flex-row align-middle items-center p-4 hover:bg-def-blue hover:text-white`
                }
              >
                {<UsergroupAddOutlined style={{ fontSize: 20 }} />}
                {/* <div className="text-lg pl-3">{label}</div> */}
              </NavLink>
            </div>
            <div
              className="flex flex-row justify-center py-5 items-center"
              onClick={() => logout()}
            >
              <img
                src="../img/exit.svg"
                style={{ width: 20, height: 20 }}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <div className="w-full">
        <header className="w-full ">
          <div className=" flex flex-row shadow-md p-2 justify-end bg-white h-16">
            <div className="flex flex-row items-center">
              <div className="mx-4 text-sm font-semibold">
                {user?.lastname?.substr(0, 1) + "." + user?.firstname}
              </div>
            </div>
          </div>
        </header>
        <div className="flex flex-col h-full bg-def-gray p-5 mt-1">
          <Routes>
            <Route key={0} path={"user"} element={<UserList />} />
            <Route key={1} path={"user/new"} element={<UserForm />} />
            <Route key={2} path={"user/edit/:id"} element={<UserForm />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default App;
